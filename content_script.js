var activeShortcuts = [];
var shortcutsList;

function clearMenu(alertIfNotFound) {
  if (document.querySelector(".services-list--shortcut")) {
    shortcutsList = document.querySelector(".services-list--shortcut");
  }
  else if (alertIfNotFound === true) {
    alert("Les raccourcis de la barre de navigation sont introuvables. Assurez-vous d'être sur un ENT Skolengo.")
  }

  while (shortcutsList.lastChild) {
    shortcutsList.removeChild(shortcutsList.lastChild);
}
}

function appendShortcut(text, url, icon) {
  shortcutsList.innerHTML += `<li class="services-list__item">
						<a href="${url}">
							<span class="icon icon--${icon}"></span>
							<span class="label">${text}</span>
						</a>
					</li>`;
  activeShortcuts.push({
    text: text,
    url: url,
    icon: icon
  })
}

function loadMenu() {
  clearMenu();
  shortcutsToAppend = JSON.parse(localStorage.getItem("customMenuShortcuts"));
  for (var i = 0; i < shortcutsToAppend.length; i++) {
    appendShortcut(shortcutsToAppend[i].text, shortcutsToAppend[i].url, shortcutsToAppend[i].icon);
  }
}

function dumpMenu() {
  browser
  localStorage.setItem("customMenuShortcuts", JSON.stringify(activeShortcuts));
}

function initialMenu() {
  clearMenu(true);
  appendShortcut("Accueil", "/sg.do?PROC=PAGE_ACCUEIL", "home-menu");
  appendShortcut("Ma classe", "javascript:alert('Non défini')", "");
  appendShortcut("Messagerie", "/sg.do?PROC=MESSAGERIE", "")
  appendShortcut("Travail à faire", "/sg.do?PROC=TRAVAIL_A_FAIRE&ACTION=AFFICHER_ELEVES_TAF", "");
  appendShortcut("Emploi du temps", "/sg.do?PROC=CDT&VUE=E", "");
  appendShortcut("Évaluations", "/sg.do?PROC=GESTION_NOTES", "");
  appendShortcut("Absences", "/sg.do?PROC=GESTION_ABSENCES", "");
  appendShortcut("Porte-documents", "/kdecole/activation_service.jsp?service=PORTEDOC", "")
}

if (localStorage.getItem("customMenuShortcuts")) {
  loadMenu();
}

browser.runtime.onMessage.addListener((message) => {
  if (message.skolengoCustomShortcutsClicked) {
    if (!localStorage.getItem("customMenuShortcuts")) {
      initialMenu();
      dumpMenu();
    }
    else {
      alert("Dans le futur (proche?), vous serez en mesure de modifier le menu ici...")
    }
  }
})


